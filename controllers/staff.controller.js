const express = require('express');
const router = express.Router();
const staffService = require('../services/staff.services');
const checkRole = require('../_helpers/checkRole')

// routes
router.get('/', getStaff)
router.post('/', checkRole.checkAdmin, createStaff);
router.put('/:id', checkRole.checkAdmin, editStaff)
router.delete('/:id', deleteStaff)
router.post('/search', checkRole.checkAdmin, searchStaff)

module.exports = router;

function createStaff(req, res, next) {
    staffService.createStaff(req, res)
}

function editStaff(req, res, next) {
    staffService.editStaff(req, res)
}

function deleteStaff(req, res, next) {
    staffService.deleteStaff(req, res)
}

function getStaff(req, res, next) {
    staffService.getStaff(req, res)
}

function searchStaff(req, res, next) {
    staffService.searchStaff(req, res)
}
