require("dotenv").config();
const { MYSQL_DB } = require("../mysql.config");

module.exports = {
    createStaff,
    editStaff,
    deleteStaff,
    getStaff,
    searchStaff
};

function createStaff(req, res) {
    const name = req.body.name || ''
    const address = req.body.address || ''
    const phone = req.body.phone || ''
    const email = req.body.email || ''
    const password = req.body.password || ''
    const role = req.body.role || ''
    const createdBy = req.body.role || 1

    if (
        name === '' || address === '' || phone === '' || email === '' || password === '' || role === ''
    ) {
        res.send({
            code: 201,
            message: "Vui lòng nhập đầy đủ thông tin"
        })
    } else {
        MYSQL_DB.connect(err => {
            let sql = "SELECT name from staff"
            MYSQL_DB.query(sql, (err, results) => {
                if (results.includes(email)) {
                    res.send({
                        code: 201,
                        message: "Địa chỉ Email đã tồn tại"
                    })
                } else {
                    let sql = `
                    INSERT INTO staff(name, address, phone, email, password, role, createdBy)
                    VALUES("${name}", "${address}", "${phone}", "${email}", "${password}", ${role}, ${createdBy})
                    `
                    MYSQL_DB.query(sql, (err, results) => {
                        if (err) {
                            res.send({
                              code: 201,
                              message: "Thao tác không thành công",
                            });
                          } else {
                            res.send({
                              code: 200,
                              message: "Thao tác thành công",
                            });
                        }
                    })
                }
            })
        })
    }
}

function editStaff(req, res) {
    const id = req.params.id
    const name = req.body.name || ''
    const address = req.body.address || ''
    const phone = req.body.phone || ''
    const email = req.body.email || ''
    const password = req.body.password || ''
    const role = req.body.role || ''

    if (
        name === '' || address === '' || phone === '' || email === '' || password === '' || role === ''
    ) {
        res.send({
            code: 201,
            message: "Vui lòng nhập đầy đủ thông tin"
        })
    } else {
        MYSQL_DB.connect(err => {
            let sql = `
            UPDATE staff
            SET name = "${name}", address = "${address}", phone = "${phone}", email = "${email}"
            WHERE staff_id = ${id}
            `
            MYSQL_DB.query(sql, (err) => {
                if (err) {
                    res.send({
                      code: 201,
                      message: "Thao tác không thành công",
                    });
                  } else {
                    res.send({
                      code: 200,
                      message: "Thao tác thành công",
                    });
                }
            })
        })
    }
}

function deleteStaff(req, res) {
    const id = req.params.id

    MYSQL_DB.connect(err => {
        let sql = `
        DELETE FROM staff WHERE staff_id = ${id}
        `
        MYSQL_DB.query(sql, (err) => {
            if (err) {
                res.send({
                  code: 201,
                  message: "Thao tác không thành công",
                });
            } else {
                res.send({
                  code: 200,
                  message: "Thao tác thành công",
                });
            }
        })
    })
}

function getStaff(req, res) {
    const id = jwt_decode(req.headers.authorization).id;

    MYSQL_DB.connect(err => {
        let sql = `SELECT * FROM staff WHERE staff_id = ${id}`
        MYSQL_DB.query(sql, (err, results) => {
            res.send({
                code: 200,
                data: results
            })
        })
    })
}

function searchStaff(req, res) {
    const key = req.body.key
        MYSQL_DB.connect(err => {
            let sql = `SELECT * FROM staff WHERE concat(staff_id, name, phone, email, address) LIKE "%${key}%"`
            MYSQL_DB.query(sql, (err, results) => {
                if (err) {
                    res.send({
                      code: 201,
                      message: "Thao tác không thành công",
                    });
                  } else {
                    res.send({
                      code: 200,
                      message: "Thao tác thành công",
                      data: results,
                    });
                  }
            })
        })
    }
