require("dotenv").config();
const { MYSQL_DB } = require("../mysql.config");

module.exports = {
  createBook,
  editBook,
  deleteBook,
  searchBook,
};

function createBook(req, res) {
  const genre_id = req.body.genre_id;
  const title = req.body.title;
  const author = req.body.author;
  const publisher = req.body.publisher;
  const edition = req.body.edition || null;
  const isbn = req.body.isbn || null;
  const pages = req.body.pages || null;
  const quantity = req.body.quantity || 0

  if (
    genre_id === undefined ||
    title === undefined ||
    author === undefined ||
    publisher === undefined
  ) {
    res.send({
      code: 201,
      message: "Vui lòng nhập đủ thông tin",
    });
  } else {
    MYSQL_DB.connect((err) => {
      let sql = `
            INSERT INTO books(genre_id, title, author, publisher, edition, isbn, pages, quantity)
            VALUES (${genre_id}, "${title}", "${author}", "${publisher}", ${edition}, "${isbn}", ${pages}, ${quantity})
            `;
      MYSQL_DB.query(sql, (err, results) => {
        if (err) {
          res.send({
            code: 201,
            message: "Thao tác không thành công",
          });
        } else {
          res.send({
            code: 200,
            message: "Thao tác thành công",
          });
        }
      });
    });
  }
}

function editBook(req, res) {
  const id = req.params.id;
  const genre_id = req.body.genre_id;
  const title = req.body.title;
  const author = req.body.author;
  const publisher = req.body.publisher;
  const edition = req.body.edition || null;
  const isbn = req.body.isbn || null;
  const pages = req.body.pages || null;
  const quantity = req.body.quantity || 0

  if (
    genre_id === undefined ||
    title === undefined ||
    author === undefined ||
    publisher === undefined
  ) {
    res.send({
      code: 201,
      message: "Vui lòng nhập đủ thông tin",
    });
  } else {
    MYSQL_DB.connect((err) => {
      let sql = `
            UPDATE books
            SET genre_id = ${genre_id}, title = "${title}", author = "${author}", publisher = "${publisher}", edition = ${edition}, isbn = "${isbn}", pages = ${pages}, quantity = ${quantity}
            WHERE book_id = ${id}
            `;
      MYSQL_DB.query(sql, (err, results) => {
        if (err) {
          res.send({
            code: 201,
            message: "Thao tác không thành công",
          });
        } else {
          res.send({
            code: 200,
            message: "Thao tác thành công",
          });
        }
      });
    });
  }
}

function deleteBook(req, res) {
  const id = req.params.id;

  MYSQL_DB.connect((err) => {
    let sql = `DELETE FROM books WHERE book_id = ${id}`;
    MYSQL_DB.query(sql, (err, results) => {
      if (err) {
        res.send({
          code: 201,
          message: "Thao tác không thành công",
        });
      } else {
        // console.log(results);
        if (results.affectedRows === 0) {
          res.send({
            code: 202,
            message: "Id khong ton tai",
          });
        } else {
          res.send({
            code: 200,
            message: "Thao tác thành công",
          });
        }
      }
    });
  });
}

function searchBook(req, res) {
  const key = req.body.key;

  MYSQL_DB.connect((err) => {
    if (key === "" || key === undefined) {
      let sql = `
      SELECT books.*, genre.name AS genre_name
      FROM books 
      INNER JOIN genre
      ON books.genre_id = genre.genre_id`;
      MYSQL_DB.query(sql, (err, results) => {
        console.log(err);
        if (err) {
          res.send({
            code: 201,
            message: "Thao tác không thành công",
          });
        } else {
          res.send({
            code: 200,
            message: "Thao tác thành công",
            data: results,
          });
        }
      });
    } else {
      let sql = `
      SELECT books.*, genre.name AS genre_name
      FROM books 
      INNER JOIN genre
      ON books.genre_id = genre.genre_id 
      WHERE concat(title, author, publisher, genre.name)
      LIKE "%${key}%"
      `;
      MYSQL_DB.query(sql, (err, results) => {
        if (err) {
          res.send({
            code: 201,
            message: "Thao tác không thành công",
          });
        } else {
          res.send({
            code: 200,
            message: "Thao tác thành công",
            data: results,
          });
        }
      });
    }
  });
}
