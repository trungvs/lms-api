require("dotenv").config();
const jwt = require("jsonwebtoken");
const jwt_decode = require("jwt-decode");
const { MYSQL_DB } = require("../mysql.config");
const omitPasswordHelper = require("../_helpers/omitPassword")

module.exports = {
  login,
  signup,
  forgot,
  changePass,
  getProfile,
  changeProfile,
  deleteProfile,
  searchProfile,
};

function login(req, res) {
  const email = req.body.email;
  const password = req.body.password;

  MYSQL_DB.connect((err) => {
    let sql = `
        SELECT * from users
        `;
    MYSQL_DB.query(sql, (err, results) => {
      if (err) {
        res.send({
          code: 201,
          message: "Đăng nhập không thành công!",
        });
      } else {
        const user = results.find(
          (u) => u.email === email && u.password === password
        );
        if (user) {
          const token = jwt.sign(
            { id: user.user_id, role: user.role },
            process.env.SECRET_KEY,
            { expiresIn: "1d" }
          );
          if (user.role === 0) {
            let sql = `
            SELECT * FROM users_info WHERE user_id = ${user.user_id}
            `
            MYSQL_DB.query(sql, (err, detailInfo) => {
              res.send({
                code: 200,
                message: "Đăng nhập thành công",
                data: {
                  ...omitPassword(user),
                  ...detailInfo[0],
                  token,
                },
              });
            })
          } else {
            res.send({
                code: 200,
                message: "Đăng nhập thành công",
                data: {
                  ...omitPassword(user),
                  token,
                },
              });
          }

        } else {
          res.send({
            code: 404,
            message: "Tài khoản hoặc mật khẩu không chính xác",
          });
        }
      }
    });
  });
}

function signup(req, res) {
  const user_code = req.body.user_code;
  const user_class = req.body.user_class;
  const name = req.body.name;
  const phone = req.body.phone;
  const email = req.body.email;
  const password = req.body.password;
  const address = req.body.address;
  const role = req.body.role;

  if (role === 0) {
    MYSQL_DB.connect((err) => {
      MYSQL_DB.connect((err) => {
        let sql = "SELECT user_code, email from users_info, users";
        MYSQL_DB.query(sql, (err, results) => {
          console.log(err);
          if (err) {
            res.send({
              code: 201,
              message: "Đăng ký không thành công",
            });
          } else {
            const user = results.find(
              (u) => u.user_code === user_code || u.email === email
            );
            if (user) {
              res.send({
                code: 201,
                message: "Mã Sinh viên hoặc Email đã được sử dụng",
              });
            } else {
              let sql = `
                INSERT INTO users (name, phone, email, password, address, role) VALUES ("${name}", "${phone}", "${email}", "${password}", "${address}", ${role})
                  `;
              MYSQL_DB.query(sql, (err, results) => {
                let sql = `INSERT INTO users_info (user_id, user_class, user_code) VALUES (LAST_INSERT_ID(), "${user_class}", "${user_code}")`
                MYSQL_DB.query(sql, (err) => {
                  if (err) {
                    res.send({
                      code: 204,
                      message: "Đăng ký không thành công",
                    });
                  } else {
                    res.send({
                      code: 200,
                      message: "Đăng ký thành công",
                    });
                  }
                })
              });
            }
          }
        });
      });
    });
  } else {
    MYSQL_DB.connect((err) => {
      MYSQL_DB.connect((err) => {
        let sql = "SELECT email from users";
        MYSQL_DB.query(sql, (err, results) => {
          if (err) {
            res.send({
              code: 201,
              message: "Đăng ký không thành công",
            });
          } else {
            const user = results.find(
              (u) => u.email === email
            );
            if (user) {
              res.send({
                code: 201,
                message: "Email đã được sử dụng",
              });
            } else {
              let sql = `
                INSERT INTO users (name, phone, email, password, address, role) VALUES ("${name}", "${phone}", "${email}", "${password}", "${address}", ${role})
                `;
              MYSQL_DB.query(sql, (err, results) => {
                if (err) {
                  res.send({
                    code: 204,
                    message: "Đăng ký không thành công",
                  });
                } else {
                  res.send({
                    code: 200,
                    message: "Đăng ký thành công",
                  });
                }
              });
            }
          }
        });
      });
    });
  }

}

function forgot(req, res) {}

function changePass(req, res) {
  const userId = jwt_decode(req.headers.authorization).id;
  const oldPassword = req.body.oldPassword;
  const newPassword = req.body.newPassword;

  MYSQL_DB.connect((err) => {
    let sql = `SELECT * FROM users WHERE user_id = ${userId}`;
    MYSQL_DB.query(sql, (err, results) => {
      if (err) {
        res.send({
          code: 201,
          message: "Thao tác không thành công",
        });
      } else if (results === []) {
        res.send({
          code: 404,
          message: "Không tồn tại người dùng này",
        });
      } else {
        if (oldPassword != results[0].password) {
          res.send({
            code: 201,
            message: "Mật khẩu cũ không chính xác",
          });
        } else if (newPassword.trim().length === 0) {
          res.send({
            code: 201,
            message: "Mật khẩu mới không được để trống",
          });
        } else {
          let sql = `
                    UPDATE users
                    SET password = "${newPassword}"
                    WHERE user_id = ${userId}
                    `;
          MYSQL_DB.query(sql, (err, results) => {
            console.log(err);
            if (!err) {
              res.send({
                code: 200,
                message: "Đổi mật khẩu thành công",
              });
            }
          });
        }
      }
    });
  });
}

function getProfile(req, res) {
  const userId = jwt_decode(req.headers.authorization).id;
  const role = jwt_decode(req.headers.authorization).role

  if (role === 0) {
    let sql = `
    SELECT users.*, users_info.user_class, users_info.user_code
    FROM users
    INNER JOIN users_info
    ON users.user_id = ${userId} && users_info.user_id = ${userId}
    `
    MYSQL_DB.query(sql, (err, results) => {
      if (err) {
        res.send({
          code: 201,
          message: "Thao tác không thành công",
        });
      } else {
        res.send({
          code: 200,
          message: "Thao tác thành công",
          data: omitPassword(results[0]),
        });
      }
    });
  } else {
    let sql = `SELECT * FROM users WHERE user_id = ${userId}`;
    MYSQL_DB.query(sql, (err, results) => {
      if (err) {
        res.send({
          code: 201,
          message: "Thao tác không thành công",
        });
      } else {
        res.send({
          code: 200,
          message: "Thao tác thành công",
          data: omitPassword(results[0]),
        });
      }
    });
  }
  }


function changeProfile(req, res) {
  const user_id = req.params.id;
  const user_code = req.body.user_code
  const user_class = req.body.user_class
  const name = req.body.name;
  const phone = req.body.phone;
  const email = req.body.email;
  const address = req.body.address;
  const role = req.body.role
  console.log(req.body)

  if (role === 0) {
    if (
      name === undefined ||
      name === "" ||
      phone === undefined ||
      phone === "" ||
      email === undefined ||
      email === "" ||
      address === undefined ||
      address === "" || 
      role === undefined ||
      role === "" ||
      user_code === undefined ||
      user_code === "" ||
      user_class === undefined ||
      user_code === ""
    ) {
      res.send({
        code: 201,
        message: "Vui lòng điền đầy đủ thông tin",
      });
    } else {
      MYSQL_DB.connect((err) => {
        let sql = `
              UPDATE users_info, users
              SET users_info.user_code = "${user_code}",
                  users_info.user_class = "${user_class}",
                  users.name = "${name}", 
                  users.phone = "${phone}", 
                  users.email = "${email}", 
                  users.address = "${address}", 
                  users.role = ${role}
              WHERE users_info.user_id = users.user_id
              AND users_info.user_id = ${user_id}
              `;
        MYSQL_DB.query(sql, (err, results) => {
          console.log(err)
          if (err) {
            res.send({
              code: 201,
              message: "Thao tác không thành công",
            });
          } else {
            res.send({
              code: 200,
              message: "Thao tác thành công",
            });
          }
        });
      });
    }
  } else {
    if (
      name === undefined ||
      name === "" ||
      phone === undefined ||
      phone === "" ||
      email === undefined ||
      email === "" ||
      address === undefined ||
      address === "" || 
      role === undefined ||
      role === ""
    ) {
      res.send({
        code: 201,
        message: "Vui lòng điền đầy đủ thông tin",
      });
    } else {
      if (role)
      MYSQL_DB.connect((err) => {
        let sql = `
              UPDATE users
              SET name = "${name}", 
                  phone = "${phone}", 
                  email = "${email}", 
                  address = "${address}", 
                  role = ${role}
              WHERE user_id = ${user_id}
              `;
        MYSQL_DB.query(sql, (err, results) => {
          console.log(err)
          if (err) {
            res.send({
              code: 201,
              message: "Thao tác không thành công",
            });
          } else {
            res.send({
              code: 200,
              message: "Thao tác thành công",
            });
          }
        });
      });
    }
  }

  
}

function deleteProfile(req, res) {
  const id = req.params.id;

  MYSQL_DB.connect((err) => {
    let sql = `SELECT * FROM users_info WHERE user_id = ${id} `
    MYSQL_DB.query(sql, (err, results) => {
      if (results.length === 0) {
        let sql = `DELETE FROM users WHERE user_id = ${id}`
        MYSQL_DB.query(sql, (err, results) => {
          if (err) {
            res.send({
              code: 201,
              message: "Thao tác không thành công",
            });
          } else {
            res.send({
              code: 200,
              message: "Thao tác thành công",
            });
          }
        })
      } else {
        let sql = `
        DELETE users.*, users_info.* 
        FROM users_info
        LEFT JOIN users
        ON users_info.user_id = users.user_id
        WHERE users_info.user_id = ${id} 
        `;
        MYSQL_DB.query(sql, (err, results) => {
          if (err) {
            res.send({
              code: 201,
              message: "Thao tác không thành công",
            });
          } else {
            if (results.affectedRows === 0) {
              res.send({
                code: 202,
                message: "Id khong ton tai",
              });
            } else {
              res.send({
                code: 200,
                message: "Thao tác thành công",
              });
            }
          }
        });
      }
    })
  });
}

function searchProfile(req, res) {
  const key = req.body.key.trim() || "";
  const role = req.body.role 

  if (role === 0) {
    MYSQL_DB.connect((err) => {
      // let sql = `
      // SELECT users.*, users_info.user_class, users_info.user_code
      // FROM users
      // WHERE users.role = 0
      // INNER JOIN users_info
      // ON users.user_id = users_info.user_id
      // WHERE concat(user_code, user_class, name, phone, email, address) 
      // LIKE "%${key}%"
      // `
      let sql = `
      SELECT users.*, users_info.user_class, users_info.user_code
      FROM users
      LEFT JOIN users_info
      ON users.user_id = users_info.user_id
      AND concat(user_code, user_class, name, phone, email, address) 
      LIKE "%${key}%"
      WHERE users.role = 0 OR users.role = 3
      `
      MYSQL_DB.query(sql, (err, results) => {
        if (err) {
          res.send({
            code: 201,
            message: "Thao tác không thành công",
          });
        } else {
          res.send({
            code: 200,
            message: "Thao tác thành công",
            data: omitPasswordHelper.omitPasswordList(results),
          });
        }
      });
    });
  } else {
    MYSQL_DB.connect(err => {
      let sql = `
      SELECT * FROM users 
      WHERE role != 0 AND role != 3
      AND concat(name, phone, email, address) 
      LIKE "%${key}%"`
      MYSQL_DB.query(sql, (err, results) => {
        if (err) {
          res.send({
            code: 201,
            message: "Thao tác không thành công",
          });
        } else {
          res.send({
            code: 200,
            message: "Thao tác thành công",
            data: omitPasswordHelper.omitPasswordList(results),
          });
        }
      })
    })
  }
}


// helper functions

function omitPassword(user) {
  const { password, ...userWithoutPassword } = user;
  return userWithoutPassword;
}

function omitPassword2(data) {
  return data.map(item => {
    const {password, ...userWithoutPassword} = item
    return userWithoutPassword
})
}